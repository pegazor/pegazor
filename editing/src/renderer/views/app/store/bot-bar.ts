import { Menu, ipcRenderer } from 'electron';
import { observable, toJS } from 'mobx';
import { IBotBarItem } from '../../../../../../pegazor/src/interfaces';
import { Store } from '../../../../../../pegazor/src/renderer/views/app/store';


/**
 * Based on BookmarkBarStore class
 */
export class BotBarStore {
  public buttonWidth = 150;
  public overflowMenu: Menu;
  private staticMainID: string;
  private store: Store;

  @observable
  public list: IBotBarItem[] = [];
  @observable
  public botBarItems: IBotBarItem[] = [];
  @observable
  public overflowItems: IBotBarItem[] = [];



  public constructor(store: Store) {
    this.store = store;
    this.load();

    this.handleWindowResize();
    ipcRenderer.on('reload-botbar-items', () => {
      this.load();
    });
  }

  
  public async load() {
    const items: IBotBarItem[] = await ipcRenderer.invoke('botbar-items-get');
    this.staticMainID = items.find((x) => x.static === 'main')._id;
    this.list = items;
    this.setBotBarItems();
  }



  public setBotBarItems(): void {
    // handle calculating how many and which bookmarks we can fit in the bar and which ones need to go in the overflow menu
    if (!this.staticMainID) return;
    const barItems: IBotBarItem[] = [];
    const overflowItems: IBotBarItem[] = [];
    const barWidth = document.body.clientWidth - 65;
    const maxChars = 18;
    let currentWidth = 0;
    const potentialItems = this.list.filter(
      ({ parent }) => parent === this.staticMainID,
    );
    potentialItems.forEach((el) => {
      if (currentWidth >= barWidth) {
        overflowItems.push(el);
      } else {
        if (el.title.length < maxChars) {
          const realWidth =
            ((this.buttonWidth - 36) / maxChars) * el.title.length;
          currentWidth += realWidth + 36;
        } else {
          currentWidth += this.buttonWidth;
        }
        if (currentWidth < barWidth) {
          barItems.push(el);
        } else {
          overflowItems.push(el);
        }
      }
    });
    this.botBarItems = barItems;
    this.overflowItems = overflowItems;
  }



  public removeItems(ids: string[]) {
    for (const id of ids) {
      const item = this.list.find((x) => x._id === id);
      const parent = this.list.find((x) => x._id === item.parent);
      parent.children = parent.children.filter((x) => x !== id);
    }
    this.list = this.list.filter((x) => !ids.includes(x._id));

    ipcRenderer.send(
      'botbar-items-remove',
      toJS(ids, { recurseEverything: true }),
    );
  }


  
  public showOverflow = (event: any) => {
    const clientRect = event.target.getBoundingClientRect();
    const y = Math.floor(clientRect.bottom) + 5;
    const x = Math.floor(clientRect.left) + 20;

    ipcRenderer.invoke(
      `show-botbar-item-dropdown-${this.store.windowId}`,
      this.staticMainID,
      toJS(this.overflowItems, { recurseEverything: true }),
      { x, y },
    );
  };



  public showFolderDropdown = (event: any, id: string) => {
    const clientRect = event.target.getBoundingClientRect();
    const y = Math.floor(clientRect.bottom) + 12;
    const x = Math.floor(clientRect.left) - 30;

    const bookmarks = toJS(this.list, { recurseEverything: true });
    ipcRenderer.invoke(
      `show-botbar-item-dropdown-${this.store.windowId}`,
      id,
      bookmarks,
      {
        x,
        y,
      },
    );
  };



  public createContextMenu(event: unknown, id: string) {
    const item = this.list.find(({ _id }) => _id === id);
    ipcRenderer.invoke(
      `show-botbar-item-context-menu-${this.store.windowId}`,
      toJS(item, { recurseEverything: true }),
    );
  }



  private handleWindowResize() {
    let debounceTimer: number;

    window.addEventListener('resize', () => {
      if (debounceTimer) {
        clearTimeout(debounceTimer);
      }
      debounceTimer = setTimeout(() => {
        this.setBotBarItems();
      }, 200);
    });
  }

}
