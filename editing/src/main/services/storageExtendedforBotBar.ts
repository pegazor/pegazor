import { ipcMain, dialog } from 'electron';
import { promises } from 'fs';
import { StorageService } from '../../../../pegazor/src/main/services/storage';
import { Application } from '../../../../pegazor/src/main/application';
import {
  IBotBarItem,
} from '../../../../pegazor/src/interfaces';



export function extendConstructorForBotBar(that: StorageService) {
  ipcMain.handle('import-botbar-items', async () => {
    const dialogRes = await dialog.showOpenDialog({
      filters: [{ name: 'BotBarItem file', extensions: ['html'] }],
    });

    try {
      const file = await promises.readFile(dialogRes.filePaths[0], 'utf8');
      return file;
    } catch (err) {
      console.error(err);
    }

    return [];
  });
/* 
  ipcMain.handle('export-botbar-items', async () => {
    await that.exportBotBarItems();
  });
 */
  ipcMain.handle('botbar-items-get', (e) => {
    return that.botBarItems;
  });

  ipcMain.on('botbar-items-remove', (e, ids: string[]) => {
    ids.forEach((x) => removeBotBarItem(x, that));
    Application.instance.windows.list.forEach((x) => {
      x.viewManager.selected.updateBotBarItem();
    });
  });

  ipcMain.handle('botbar-items-add', async (e, item) => {
    const b = await addBotBarItem(item, that);

    Application.instance.windows.list.forEach((x) => {
      x.viewManager.selected.updateBotBarItem();
    });

    return b;
  });

  ipcMain.handle('botbar-items-get-folders', async (e) => {
    return that.botBarItems.filter((x) => x.isFolder);
  });

  ipcMain.on('botbar-item-update', async (e, id, change) => {
    await updateBotBarItem(id, change, that);
  });
}



export async function loadBotBarItems(that: StorageService) {
  const items = await that.find<IBotBarItem>({ scope: 'botBarItems', query: {} });

  items.sort((a, b) => a.order - b.order);

  let barFolder = items.find((x) => x.static === 'main');

  that.botBarItems = items;

  if (!barFolder) {
    barFolder = await addBotBarItem({
      static: 'main',
      isFolder: true,
    }, that);

    for (const item of items) {
      if (!item.static) {
        await updateBotBarItem(item._id, { parent: barFolder._id }, that);
      }
    }
  }

}



export async function addBotBarItem(item: IBotBarItem, that: StorageService): Promise<IBotBarItem> {
  if (item.parent === undefined) {
    item.parent = null;
  }

  if (item.parent === null && !item.static) {
    throw new Error('Parent botBarItem should be specified');
  }

  if (item.isFolder) {
    item.children = item.children || [];
  } else {
  }

  if (item.order === undefined) {
    item.order = that.botBarItems.filter((x) => !Boolean(x.static)).length;
  }

  const doc = await that.insert<IBotBarItem>({ item, scope: 'botBarItems' });

  if (item.parent) {
    const parent = that.botBarItems.find((x) => x._id === item.parent);
    await updateBotBarItem(parent._id, {
      children: [...parent.children, doc._id],
    }, that);
  }

  that.botBarItems.push(doc);

  Application.instance.windows.broadcast('reload-botbar-items');

  return doc;
}



export function removeBotBarItem(id: string, that: StorageService) {
  const item = that.botBarItems.find((x) => x._id === id);

  if (!item) return;

  that.botBarItems = that.botBarItems.filter((x) => x._id !== id);
  const parent = that.botBarItems.find((x) => x._id === item.parent);

  parent.children = parent.children.filter((x) => x !== id);
  updateBotBarItem(item.parent, { children: parent.children }, that);

  that.remove({ scope: 'botBarItems', query: { _id: id } });

  if (item.isFolder) {
    that.botBarItems = that.botBarItems.filter((x) => x.parent !== id);
    const removed = that.botBarItems.filter((x) => x.parent === id);

    that.remove({ scope: 'botBarItems', query: { parent: id }, multi: true });

    for (const i of removed) {
      if (i.isFolder) {
        removeBotBarItem(i._id, that);
      }
    }
  }
  Application.instance.windows.broadcast('reload-botbar-items');
}



export async function updateBotBarItem(id: string, change: IBotBarItem, that: StorageService) {
  const index = that.botBarItems.indexOf(
    that.botBarItems.find((x) => x._id === id),
  );
  that.botBarItems[index] = { ...that.botBarItems[index], ...change };

  await that.update({
    scope: 'botBarItems',
    query: { _id: id },
    value: change,
  });

  if (change.parent) {
    const parent = that.botBarItems.find((x) => x._id === change.parent);
    if (!parent.children.includes(change._id))
      await updateBotBarItem(parent._id, {
        children: [...parent.children, change._id],
      }, that);
  }

  Application.instance.windows.broadcast('reload-botbar-items');
}