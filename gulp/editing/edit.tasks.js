function edit(cb, EDITTSX_FILE_PATTERN, EDITTSX_FILE_BASE, EDITTSX_TARGET_BASE, REPLACE_FILE_EXT_FROM, REPLACE_FILE_EXT_TO, replace, gulpData, through, gulp, path, fs) {

  gulp
    .src(EDITTSX_FILE_PATTERN, {base: './'})
    .pipe(
      gulpData(file => JSON.parse(fs.readFileSync(file.path)))
    )
    .pipe(through.obj((vinylFile, encoding, callback) => {
      const fileName = path.relative(EDITTSX_FILE_BASE, vinylFile.relative).replace(REPLACE_FILE_EXT_FROM, REPLACE_FILE_EXT_TO);
      const target = path.join(EDITTSX_TARGET_BASE, fileName);

      console.log(`edited: ${target}`);

      let src = gulp.src(target, {base: './'})

      vinylFile.data.replace.forEach((params) => {
        let { regex, replaceWith } = params;

        if (Array.isArray(regex)) {
          regex = regex.join(`\\s+`);
        }

        if (Array.isArray(replaceWith)) {
          replaceWith = replaceWith.join(`\n`);
        }

        console.log({ regex, replaceWith });

        src = src.pipe(
          replace(new RegExp(regex), replaceWith)
        );
        
      });

      src.pipe(gulp.dest('./'));
      

      callback(null, vinylFile);
    }))
    
  cb();
}



function config_edit(EDITTSX_FILE_PATTERN, EDITTSX_FILE_BASE, EDITTSX_TARGET_BASE, REPLACE_FILE_EXT_FROM, REPLACE_FILE_EXT_TO) {
  return (cb) => {
    const fs = require('fs');
    const path = require('path');
    const gulp = require('gulp');
    const through = require('through2');
    const replace = require('gulp-replace');
    const gulpData = require('gulp-data');

    edit(cb, EDITTSX_FILE_PATTERN, EDITTSX_FILE_BASE, EDITTSX_TARGET_BASE, REPLACE_FILE_EXT_FROM, REPLACE_FILE_EXT_TO, replace, gulpData, through, gulp, path, fs);
  };
}



module.exports = {
  config_edit
};
