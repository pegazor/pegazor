function copyFile(cb, COPY_FILE_PATTERN, COPY_FILE_BASE, COPY_TARGET_BASE, gulp) {
  const print = require('gulp-print').default;

  gulp
    .src(COPY_FILE_PATTERN, { base: COPY_FILE_BASE })
    .pipe(print())
    .pipe(gulp.dest(COPY_TARGET_BASE));

  cb();
}



function config_copyFile(COPY_FILE_PATTERN, COPY_FILE_BASE, COPY_TARGET_BASE) {
  return (cb) => {
    const gulp = require('gulp');

    copyFile(cb, COPY_FILE_PATTERN, COPY_FILE_BASE, COPY_TARGET_BASE, gulp);
  }
}



module.exports = {
  config_copyFile
};
