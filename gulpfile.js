const path = require('path');
const fs = require('fs');

const { series } = require('gulp');

const {
  config_getWexond_task,
  config_makeACopyOfWexond
} = require('./gulp/wexond/wexond.tasks');

const {
  config_editJSON
} = require('./gulp/editing/editJSON.tasks');
const {
  config_edit
} = require('./gulp/editing/edit.tasks');
const {
  config_copyFile
} = require('./gulp/editing/copyFile.tasks');

const {
  config_installDependencies
} = require('./gulp/pegazor/dependencies/dependencies.tasks');


const configfile_path = './gulp/config.js';
const config = require(configfile_path);







const getWexond = config_getWexond_task(config.WEXOND_REPO_PATH, config.WEXOND_DIR_PATH);
const makeACopyOfWexond = config_makeACopyOfWexond(config.WEXOND_DIR_PATH, config.PEGAZOR_DIR_PATH);


const editJSON = config_editJSON(config.EDITJSON_FILE_PATTERN, config.EDITJSON_FILE_BASE, config.EDITJSON_TARGET_BASE);
const editJS   = config_edit( config.EDITJS_FILE_PATTERN,   config.EDITJS_FILE_BASE,   config.EDITJS_TARGET_BASE,  '.editJS.json',  '.js');
const editTS   = config_edit( config.EDITTS_FILE_PATTERN,   config.EDITTS_FILE_BASE,   config.EDITTS_TARGET_BASE,  '.editTS.json',  '.ts');
const editTSX  = config_edit( config.EDITTSX_FILE_PATTERN,  config.EDITTSX_FILE_BASE,  config.EDITTSX_TARGET_BASE, '.editTSX.json', '.tsx');
const copyFile = config_copyFile(config.COPY_FILE_PATTERN,     config.COPY_FILE_BASE,     config.COPY_TARGET_BASE);
const installDependencies = config_installDependencies(config.PEGAZOR_PACKAGE_PATH);



exports.getWexond = getWexond;
exports.makeACopyOfWexond = makeACopyOfWexond;
exports.editJSON = editJSON;
exports.editJS   = editJS;
exports.editTS   = editTS;
exports.editTSX  = editTSX;
exports.copyFile = copyFile;
exports.installDependencies = installDependencies;

exports.postinstall = series(getWexond);
exports.transform = series(editJSON, editJS, editTS, editTSX, copyFile);
exports.fromWexond = series(makeACopyOfWexond, exports.transform, installDependencies);