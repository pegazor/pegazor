import { BrowserWindow } from 'electron';
import { Application } from '../../../../pegazor/src/main/application';
import { DIALOG_MARGIN_TOP, DIALOG_MARGIN } from '../../../../pegazor/src/constants/design';
import { IBotBarItem } from '../../../../pegazor/src/interfaces';

export const showAddBotBarItemDialog = (
  browserWindow: BrowserWindow,
  x: number,
  y: number,
  data?: {
    url: string;
    title: string;
    botBarItem?: IBotBarItem;
    favicon?: string;
  },
) => {
  if (!data) {
    const {
      url,
      title,
      botBarItem,
      favicon,
    } = Application.instance.windows.current.viewManager.selected;
    data = {
      url,
      title,
      botBarItem,
      favicon,
    };
  }

  const dialog = Application.instance.dialogs.show({
    name: 'add-botbar-item',
    browserWindow,
    getBounds: () => ({
      width: 366,
      height: 240,
      x: x - 366 + DIALOG_MARGIN,
      y: y - DIALOG_MARGIN_TOP,
    }),
    onWindowBoundsUpdate: () => dialog.hide(),
  });

  if (!dialog) return;

  dialog.on('loaded', (e) => {
    e.reply('data', data);
  });
};
