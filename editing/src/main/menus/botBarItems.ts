import {
  Menu,
  nativeImage,
  NativeImage,
  MenuItemConstructorOptions,
  app,
} from 'electron';
import { join } from 'path';
import { IBotBarItem } from '../../../../pegazor/src/interfaces';
import { Application } from '../../../../pegazor/src/main/application';
import { AppWindow } from '../../../../pegazor/src/main/windows/app';
import { showAddBotBarItemDialog } from '../../../../pegazor/src/main/dialogs/add-botbar-item';
import { removeBotBarItem } from '../../../../pegazor/src/main/services/storageExtendedforBotBar';

function getPath(file: string) {
  if (process.env.NODE_ENV === 'development') {
    return join(
      app.getAppPath(),
      'src',
      'renderer',
      'resources',
      'icons',
      `${file}.png`,
    );
  } else {
    const path = require(`~/renderer/resources/icons/${file}.png`);
    return join(app.getAppPath(), `build`, path);
  }
}

function getIcon(
  favicon: string | undefined,
  isFolder: boolean,
): NativeImage | string {
  return getPath('clicking');
  
  if (Application.instance.settings.object.theme === 'wexond-dark') {
    if (isFolder) {
      return getPath('folder_light');
    } else {
      return getPath('page_light');
    }
  } else {
    if (isFolder) {
      return getPath('folder_dark');
    } else {
      return getPath('page_dark');
    }
  }
}

export function createDropdown(
  appWindow: AppWindow,
  parentID: string,
  botBarItems: IBotBarItem[],
): Electron.Menu {
  const folderBotBarItems = botBarItems.filter(
    ({ static: staticName, parent }) => !staticName && parent === parentID,
  );
  const template = folderBotBarItems.map<MenuItemConstructorOptions>(
    ({ title, url, favicon, isFolder, _id }) => ({
      click: url
        ? () => {
            appWindow.viewManager.create({ url, active: true });
          }
        : undefined,
      label: title,
      icon: getIcon(favicon, isFolder),
      submenu: isFolder ? createDropdown(appWindow, _id, botBarItems) : undefined,
      id: _id,
    }),
  );

  return template.length > 0
    ? Menu.buildFromTemplate(template)
    : Menu.buildFromTemplate([{ label: '(empty)', enabled: false }]);
}

export function createMenu(appWindow: AppWindow, item: IBotBarItem) {
  const { isFolder, url } = item;
  const folderItems: MenuItemConstructorOptions[] = [];

  const template: MenuItemConstructorOptions[] = [
    ...(!isFolder ? folderItems : []),
    {
      label: 'Edit',
      click: () => {
        const windowBounds = appWindow.win.getBounds();
        showAddBotBarItemDialog(appWindow.win, windowBounds.width - 20, 72, {
          url: item.url,
          title: item.title,
          botBarItem: item,
          favicon: item.favicon,
        });
      },
    },
    {
      label: 'Delete',
      click: () => {
        const storage = Application.instance.storage;
        
        removeBotBarItem(item._id, storage);
      },
    },
  ];

  return Menu.buildFromTemplate(template);
}
