function editJSON(cb, EDITJSON_FILE_PATTERN, EDITJSON_FILE_BASE, EDITJSON_TARGET_BASE, gulpData, jeditor, through, gulp, path, fs) {
  


  gulp
    .src(EDITJSON_FILE_PATTERN, {base: './'})
    .pipe(
      gulpData(file => JSON.parse(fs.readFileSync(file.path)))
    )
    .pipe(
      through.obj((vinylFile, encoding, callback) => {
        const fileName = path.relative(EDITJSON_FILE_BASE, vinylFile.relative).replace('.editJSON.json', '.json');
        const target = path.join(EDITJSON_TARGET_BASE, fileName);

        console.log(`edited: ${target}`);
        
        gulp
        .src(target, {base: './'})
        .pipe(
          jeditor(vinylFile.data)
        )
        .pipe(
          gulp.dest('./')
        );


        callback(null, vinylFile);
      })
    );

  cb();
}


function config_editJSON(EDITJSON_FILE_PATTERN, EDITJSON_FILE_BASE, EDITJSON_TARGET_BASE) {
  return (cb) => {
    const fs = require('fs');
    const path = require('path');
    const gulp = require('gulp');
    const through = require('through2');
    const gulpData = require('gulp-data');
    const jeditor = require('gulp-json-editor');

    editJSON(cb, EDITJSON_FILE_PATTERN, EDITJSON_FILE_BASE, EDITJSON_TARGET_BASE, gulpData, jeditor, through, gulp, path, fs);
  }
}



module.exports = {
  config_editJSON,
}
