import { ipcRenderer } from 'electron';
import { observable } from 'mobx';
import { IBotBarItem } from '../../../../../../pegazor/src/interfaces';
import * as React from 'react';
import { DialogStore } from '../../../../../../pegazor/src/models/dialog-store';

export class Store extends DialogStore {
  @observable
  public folders: IBotBarItem[] = [];

  @observable
  public dialogTitle = '';

  public titleRef = React.createRef<HTMLInputElement>();

  public botBarItem: IBotBarItem;

  @observable
  public currentFolder: IBotBarItem;

  public constructor() {
    super();

    (async () => {
      this.folders = await ipcRenderer.invoke('botbar-items-get-folders');
      this.currentFolder = this.folders.find((x) => x.static === 'main');
    })();

    ipcRenderer.on('data', async (e, data) => {
      const { botBarItem, title, url, favicon } = data;

      if (!botBarItem) {
        this.dialogTitle = !botBarItem ? 'BotBarItem added' : 'Edit botBarItem';
      }

      this.botBarItem = botBarItem;
      this.folders = await ipcRenderer.invoke('botbar-items-get-folders');

      if (!this.botBarItem) {
        this.botBarItem = await ipcRenderer.invoke('botbar-items-add', {
          title,
          url,
          favicon,
          parent: this.folders.find((x) => x.static === 'main')._id,
        });
      }

      this.currentFolder = this.folders.find(
        (x) => x._id === this.botBarItem.parent,
      );

      if (this.titleRef.current) {
        this.titleRef.current.value = title;
        this.titleRef.current.focus();
        this.titleRef.current.select();
      }
    });
  }
}

export default new Store();
