import parse from 'node-bookmarks-parser';

import { IBotBarItem } from '../../../../../../pegazor/src/interfaces';
import store from '../store';

export const getBotBarItemTitle = (item: IBotBarItem) => {
  if (!item.static) return item.title;

  if (item.static === 'main') {
    return 'Bot bar';
  }

  return '';
};

export const addImported = async (
  arr: ReturnType<typeof parse>,
  parent: IBotBarItem = null,
) => {
  let order = 0;

  for (const item of arr) {
    if (item.nsRoot) {
      let folder: IBotBarItem = null;

      if (item.nsRoot === 'toolbar') {
        folder = store.list.find((x) => x.static === 'main');
      } else {
        folder = store.list.find((x) => x.static === 'other');
      }

      if (folder) {
        addImported(item.children, folder);
      }

      return;
    }

    const botBarItem = await store.addItem({
      isFolder: item.type === 'folder',
      title: item.title,
      url: item.url,
      parent: parent && parent._id,
      children: [],
      favicon: item.icon,
      order,
    });

    if (parent) {
      parent.children.push(botBarItem._id);
    }

    if (botBarItem.isFolder) {
      addImported(item.children, botBarItem);
    }

    order++;
  }
};
