


function getWexond_task(cb, git, WEXOND_REPO_PATH, WEXOND_DIR_PATH) {
  git.clone(
    WEXOND_REPO_PATH,
    {args: WEXOND_DIR_PATH},
    (err) => {
      if (err) console.warn(err.message);
      cb();
    }
  );
}



function config_getWexond_task(WEXOND_REPO_PATH, WEXOND_DIR_PATH) {
  return (cb) => {
    const git = require('gulp-git');

    getWexond_task(cb, git, WEXOND_REPO_PATH, WEXOND_DIR_PATH);
  }
}




function makeACopyOfWexond(cb, WEXOND_DIR_PATH, PEGAZOR_DIR_PATH, gulp) {
  gulp
  .src([
    `${WEXOND_DIR_PATH}/**/*`,
    `${WEXOND_DIR_PATH}/**/.*` // Hidden files
  ])
  .pipe(
    gulp.dest(PEGAZOR_DIR_PATH)
  );

  cb();
}



function config_makeACopyOfWexond(WEXOND_DIR_PATH, PEGAZOR_DIR_PATH) {
  return (cb) => {
    const gulp = require('gulp');

    makeACopyOfWexond(cb, WEXOND_DIR_PATH, PEGAZOR_DIR_PATH, gulp);
  }
}



module.exports = {
  config_getWexond_task,
  config_makeACopyOfWexond
}