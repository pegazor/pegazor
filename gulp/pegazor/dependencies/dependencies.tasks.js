

function installDependencies(cb, PEGAZOR_PACKAGE_PATH, install, gulp) {
  gulp
  .src([ PEGAZOR_PACKAGE_PATH ])
  .pipe(install());

  cb();
}



function config_installDependencies(PEGAZOR_PACKAGE_PATH) {
  return (cb) => {
    const gulp = require("gulp");
    const install = require("gulp-install");

    installDependencies(cb, PEGAZOR_PACKAGE_PATH, install, gulp)
  }
}



module.exports = {
  config_installDependencies
};
