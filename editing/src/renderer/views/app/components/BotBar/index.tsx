import { ipcRenderer } from 'electron';
import * as React from 'react';
import store from '../../../../../../../pegazor/src/renderer/views/app/store';
import { observer } from 'mobx-react-lite';
import {
  BotBarButton,
  BotBar as StyledBotBar,
  BotBarItemSection,
  Favicon,
  Title,
} from './style';
import { ToolbarButton } from '../../../../../../../pegazor/src/renderer/views/app/components/ToolbarButton';
import {
  ICON_FOLDER,
  ICON_PAGE,
  ICON_ARROW_RIGHT,
  ICON_CLICKING,
  ICON_CLICKING3,
  ICON_CIRCLE,
} from '../../../../../../../pegazor/src/renderer/constants/icons';
import { IBotBarItem } from '../../../../../../../pegazor/src/interfaces';

type BotBarItemProps = {
  title: string;
  url: string;
  favicon?: string;
  isFolder: boolean;
  id: string;
};

const BotBarItem = observer(
  ({ title, url, favicon, isFolder, id }: BotBarItemProps) => {
    const { buttonWidth } = store.botBar;

    function onClick(event: any) {
      if (url) {
        ipcRenderer.send(`add-tab-${store.windowId}`, {
          url,
          active: true,
        });
      } else {
        store.botBar.showFolderDropdown(event, id);
      }
    }

    function onContextMenu(event: any) {
      store.botBar.createContextMenu(event, id);
    }
    return (
      <BotBarButton
        dense
        width={buttonWidth}
        theme={store.theme}
        toggled={false}
        disabled={false}
        onClick={onClick}
        onContextMenu={onContextMenu}
      >
        <Favicon
          style={{
            backgroundImage: `url(${
              {
                circle: ICON_CIRCLE,
                clicking: ICON_CLICKING,
                'clicking-3': ICON_CLICKING3
              }[favicon]
            })`,
            filter:
              store.theme['pages.lightForeground']
                ? 'invert(100%)'
                : 'none',
          }}
        />
        <Title>{title}</Title>
      </BotBarButton>
    );
  },
);

export const BotBar = observer(() => {
  const { botBarItems: list, showOverflow } = store.botBar;

  return store.settings.object.botBar ? (
    <StyledBotBar>
      <BotBarItemSection>
        {list.map(({ title, url, favicon, _id, isFolder }: IBotBarItem) => (
          <BotBarItem
            key={_id}
            id={_id}
            title={title}
            url={url}
            favicon={favicon}
            isFolder={isFolder}
          />
        ))}
      </BotBarItemSection>
      {store.botBar.overflowItems.length > 0 && (
        <ToolbarButton icon={ICON_ARROW_RIGHT} onClick={showOverflow} />
      )}
    </StyledBotBar>
  ) : null;
});
