exports.WEXOND_DIR_PATH = 'wexond';
exports.WEXOND_REPO_PATH = 'https://github.com/wexond/desktop.git';

exports.PEGAZOR_DIR_PATH = 'pegazor';
exports.PEGAZOR_PACKAGE_PATH = 'pegazor/package.json';

exports.EDITJSON_FILE_PATTERN = 'editing/**/*.editJSON.json';
exports.EDITJSON_FILE_BASE    = 'editing';
exports.EDITJSON_TARGET_BASE  = 'pegazor';

exports.EDITTSX_FILE_PATTERN  = 'editing/**/*.editTSX.json';
exports.EDITTSX_FILE_BASE     = 'editing';
exports.EDITTSX_TARGET_BASE   = 'pegazor';

exports.EDITTS_FILE_PATTERN  = 'editing/**/*.editTS.json';
exports.EDITTS_FILE_BASE     = 'editing';
exports.EDITTS_TARGET_BASE   = 'pegazor';

exports.EDITJS_FILE_PATTERN  = 'editing/**/*.editJS.json';
exports.EDITJS_FILE_BASE     = 'editing';
exports.EDITJS_TARGET_BASE   = 'pegazor';

// Should have every not-to-copy pattern defined
exports.COPY_FILE_PATTERN  = [
  'editing/**/*.*',
  `!${exports.EDITJSON_FILE_PATTERN}`,
  `!${exports.EDITJS_FILE_PATTERN }`,
  `!${exports.EDITTS_FILE_PATTERN }`,
  `!${exports.EDITTSX_FILE_PATTERN }`
];
exports.COPY_FILE_BASE     = 'editing';
exports.COPY_TARGET_BASE   = 'pegazor';
