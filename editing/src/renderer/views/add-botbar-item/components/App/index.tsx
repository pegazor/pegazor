import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { ThemeProvider } from 'styled-components';
import { hot } from 'react-hot-loader/root';

import { StyledApp, Title, Row, Label, Buttons } from './style';
import store from '../../store';
import { Input, Dropdown } from '../../../../../../../pegazor/src/renderer/components/Input';
import { Button } from '../../../../../../../pegazor/src/renderer/components/Button';
import { ipcRenderer, remote } from 'electron';
import { getBotBarItemTitle } from '../../../../../../../pegazor/src/renderer/views/botBarItems/utils';
import { UIStyle } from '../../../../../../../pegazor/src/renderer/mixins/default-styles';

const onDone = () => {
  store.hide();
};

const updateBotBarItem = () => {
  if (!store.botBarItem) return;
  ipcRenderer.send('botbar-item-update', store.botBarItem._id, store.botBarItem);
};

const onChange = () => {
  store.botBarItem.title = store.titleRef.current.value;
  updateBotBarItem();
};

const onDropdownClick = (e: React.MouseEvent<HTMLDivElement>) => {
  const { left, top, height } = e.currentTarget.getBoundingClientRect();
  const menu = remote.Menu.buildFromTemplate([
    ...store.folders.map((folder) => ({
      label: getBotBarItemTitle(folder),
      click: () => {
        store.currentFolder = folder;
        store.botBarItem.parent = folder._id;
        updateBotBarItem();
      },
    })),
  ]);

  const { x, y } = remote.BrowserView.fromWebContents(
    remote.getCurrentWebContents(),
  ).getBounds();

  menu.popup({ x: x + left, y: y + top + height });
};

const onRemove = () => {
  if (!store.botBarItem) return;
  ipcRenderer.send('botbar-items-remove', [store.botBarItem._id]);
  store.hide();
};

export const App = hot(
  observer(() => {
    return (
      <ThemeProvider theme={{ ...store.theme }}>
        <StyledApp visible={store.visible}>
          <UIStyle />
          <Title>{store.dialogTitle}</Title>
          <Row>
            <Label>Name</Label>
            <Input
              tabIndex={0}
              className="textfield"
              ref={store.titleRef}
              onChange={onChange}
            />
          </Row>
          <Buttons>
            <Button onClick={onDone}>Done</Button>
            <Button
              onClick={onRemove}
              background={
                store.theme['dialog.lightForeground']
                  ? 'rgba(255, 255, 255, 0.08)'
                  : 'rgba(0, 0, 0, 0.08)'
              }
              foreground={
                store.theme['dialog.lightForeground'] ? 'white' : 'black'
              }
            >
              Remove
            </Button>
          </Buttons>
        </StyledApp>
      </ThemeProvider>
    );
  }),
);
